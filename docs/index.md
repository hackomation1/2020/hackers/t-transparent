# 1. Project Title: Electronic key

Een team consisting of 5 people, all mechanical engineers students, created the electronic key (project) to showcase how you can open a lock/door, * without physically touching the door handle or having the actual key.* 
The project is intended to make life easier and more efficient by not spending to much time searching for your keys because you lost them and it's safer due to the COVID-19 pandemic.

## Overview & Features

Features:
 
1. **Solenoid** lock  (DC 12V Electronic Door Lock Solenoid Valve Lock Widely Used Symmetrical Design Power on to Unlock Safety   Function Lock)
2. **RFID** tag (RFID Kit Mifare RC522 RFID Reader Module with S50 White Card and Key Ring for Arduino Raspberry Pi)
3. **Membrane Keypad Module** (4x4 Matrix 16 Key Membrane Switch Keypad Keyboard for Arduino & Raspberry Pi)
4. **Ultrasonic Proximity Sensor** (ELEGOO 5PCS HC-SR04 Ultrasonic Module Distance Sensor for Arduino UNO MEGA2560 Nano Robot XBee ZigBee)
5. **Pizo Electric Buzzer**
6. **Relay**
7. **LED lights 4x**( 2x Red + Green + blue)
8. **Bread board**
9. **Battery Lithium Lionn**
10. **Jumper cables**
11. **Resistors**
12. **USB Cable**
13. **12 V supply**
14. **ESP32 Development board**


## Demo / Proof of work

A video of our project working:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xb4-GQ3cfko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Deliverables

- Project poster

![t-transparent](img/Poster.png)

-Projet Presentation:

[link](https://drive.google.com/file/d/1cAO1Nsaur5T2L-pyrDwAH_rJg2mLJF9o/view?usp=sharing)
[link](https://drive.google.com/file/d/1V5CZn4e4UVXFwt5Qazxba2b5ltUyqmhA/view?usp=sharing)

- Product pitch deck

[link](https://canvanizer.com/canvas/rA0e2u8NFcvSt)

- Proof of concept working

<iframe width="560" height="315" src="https://www.youtube.com/embed/B0Y285QihqU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/gm8eMs2vqck" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## the team & contact

< Time to shine with your team .. show their expertise and contribution to the project >

Shailesh
1.5 Lectures attended
2. Attend all 8 group meetings
3. Came up with the project idea 10/10
4. Order placed 10/10
4. Working on 10/10 programming
5. Work on the construction of the 10/10 circuits
6. Help documents 1/10
7. communicate with Theo 9.5 / 10
8. Communicate with Jullie 7/10
9. 1st pitch slides make 10/10
10 Attach circuit to enclosure 6/10
11.2nd pitch slides 8/10
12.Final pitch to present 10/10

Sabrina
1.4 lectures attended
2. Present at all 8 group meetings
3. Working on Gitlab and Github 10/10
4. Documentation 9/10
5. work on poster 5/10
6. Attach circuit to enclosure 1/10
7. Collaborate on enclosure 1/10
8. Help make canvas 5/10
9. Communicating with Jullie 1/10

Jimmy
  1.5 lectures
  2. Attend all 8 group meetings
  3.1st pitch presented 10/10
  4. Box design 4/10
  5. Working on 1st enclosure 10/10
  6. Attach circuit to enclosure 4/10
  7. Working on 2nd and official enclosure (material and labor input) 8/10

Cherryl
 1. 5 lectures
 2. Attend all 8 group meetings
 3. 2nd pitch present 10/10
 4. Cooperate on enclosure 1/10
 5. Communicate with You 1/10
 6. Communicate with Theo 0.5 / 10
 7. Box design 4/10
 8.  Communicate with Theo 0.5 / 10
 9.  Help make canvas 5/10
 10. Working on poster 5/10
 11. Communicate with Jullie 1.5 / 10


Santoesha
 1. Attended 3 lectures
 2. Attend  5 group meetings
 3. Box design 4/10
 4.2e pitch slides make 8/10
 5. Communicate with Jullie 0.5 / 10












