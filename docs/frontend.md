# 3. Front-End & Platform

The posibility of opening and closing  a door lock whenever the user want with internet (IOT).

## 3.1 Objectives

For people who are deaf and blind or people with disabilities, who can´t get to the fast enough or struggle with the keys. Having the option to unlock their door lock via their phone. Using IOT we can make this option true.

## 3.3 Steps taken

Stappen voor IOT:
1. Create an HTML file - We used one  existing file where you can turn a LED ON and OFF.

2. Create Cpp file -We used one existing file where you can turn a LED ON and OFF.

3. Create folder named Data.

4. Use code From Async WebServer ESP32 , and paste whole project code U_R in this.

5. Install libraries  as SPIFF. So the Esp32 when it commands to make the  Green led ON ,the lock also opens.

6. If Libraries SPIFF not present go to site and dowload libraries and paste in notepad and save as h and cpp files.

7. Install WIFI libraries. Go to site and save the files as h and cpp files <https://randomnerdtutorials.com/esp32-async-web-server-espasyncwebserver-library/>

8. Important to have these 3 libraries also 
 hashtag include <WiFi.h>
 hashtag include <AsyncTCP.h>
 hashtag include <ESPAsyncWebServer.h>

9. Setting your network credentials.
Insert in project code your network credentials in the following variables ssid and password, so that the ESP32 can connect to your local network.

10. Input Parameters
To check the parameters passed on the URL (GPIO number and its state), we create two variables, one for the output and other for the state.
const char* PARAM_INPUT_1 = "output";
const char* PARAM_INPUT_2 = "state";

11. Create Folder ESP32_Async_Web_Server and paste Data folder en final project Code with embedded part. In Data folder HTML file and CPP file must be present .
 
Upload code to ESP32 board. 
12. Upload data to ESP32 board .

13. Resest.

14. Press enable.

15. Read Serial Monitor , when the ESP32 connects to WIFI it displays an IP adress.

16. Go to the IP-adress on your mobile phone ,there you wil see to states. ON and OFF for the LED.

17. By pressing for the LED on and OFF, the green LED goes ON and OFF and so the Lock also switches ON and OFF.

18. The lock signal pin was in serie plugged in on the bread board with the LED pin.

## 3.4 Testing & Problems

the only thing during testing is that, we discovered that you must have a good internet connection.

The IOT is safer than Blynk, because Blynk is a third party app is.
Ours is a end to end IOT solution.

## 3.5 Proof of work

Video using your phone:

<iframe width="560" height="315" src="https://www.youtube.com/embed/21nDYgpF09w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/xL4IFdsT9EU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3.6 Files & Code

This is the code for the IOT en doorlock combined.

```
#include <SPI.h>
#include <MFRC522.h>
#include <ESP32Servo.h>


//  Required libraries
#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"

// Replace with your network credentials
const char* ssid = "Mohan";
const char* password = "11101110";

// Set LED GPIO
const int ledPin = 5;
// Stores LED state
String ledState;



//ULTRASONIC
int TRIG_PIN  = 17; // Arduino pin connected to Ultrasonic Sensor's TRIG pin
int ECHO_PIN  = 34; // Arduino pin connected to Ultrasonic Sensor's ECHO pin
int DISTANCE_THRESHOLD = 10; // centimeters

// variables will change:
float duration_us, distance_cm;

//LED & LOCK PIN DEF 
#define SS_PIN     21
#define RST_PIN    22
#define LED_G      5 //define green LED pin
#define LED_R      4 //define red LED
#define  BUZZER     2  //BUZZER pin
#define LOCK_PIN     15 //define red LED

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.


AsyncWebServer server(80);

// Replaces placeholder with LED state value
String processor(const String& var){
  Serial.println(var);
  if(var == "STATE"){
    if(digitalRead(ledPin)){
      ledState = "ON";
    }
    else{
      ledState = "OFF";
    }
    Serial.print(ledState);
    return ledState;
  }
  return String();
}
 
void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);
// ### insert
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  //input_password.reserve(32); // maximum input characters is 33, change if needed

  //Led and & Lock def
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  
  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);
  pinMode(BUZZER, OUTPUT);

  //ULTRASONIC
  pinMode(TRIG_PIN, OUTPUT);  // set arduino pin to output mode
  pinMode(ECHO_PIN, INPUT);   // set arduino pin to input mode
  // ###

  // Initialize SPIFFS
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });

  // Route to set GPIO to HIGH
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, HIGH);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to set GPIO to LOW
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, LOW);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Start server
  server.begin();
}
 
void handle_swipe() {
  // generate 10-microsecond pulse to TRIG pin
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  //delay(5000);

  // measure duration of pulse from ECHO pin
  duration_us = pulseIn(ECHO_PIN, HIGH);
  // calculate the distance
  distance_cm = 0.017 * duration_us;

  if(distance_cm < DISTANCE_THRESHOLD && distance_cm >1)
  {
      Serial.println("ULTRA Authorized access");
      digitalWrite(LED_G, HIGH);
      //delay(500);
      //digitalWrite(LED_B, HIGH);
      //delay(500);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      delay(500);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      Serial.println("LOCK RESET");
      delay(5000);

  }
}


void handle_rfid(){
  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent()) 
  {
    // Select one of the cards
    if ( mfrc522.PICC_ReadCardSerial()) 
    {
  
    //Show UID on serial monitor
    Serial.print("UID tag :");
    String content= "";
    byte letter;
    for (byte i = 0; i < mfrc522.uid.size; i++) 
    {
       Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
       Serial.print(mfrc522.uid.uidByte[i], HEX);
       content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
       content.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Message : ");
    content.toUpperCase();
    if (content.substring(1) == "59 F7 D8 C2") //change here the UID of the card/cards that you want to give access
    {
      Serial.println("Authorized access");
      digitalWrite(LED_G, HIGH);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      //myServo.write(180);
      delay(1000);
      //myServo.write(0);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      Serial.println("LOCK RESET");
      mfrc522.PCD_Init();   // Initiate MFRC522
    
  
    }
   else   {
      Serial.println(" Access denied");
      digitalWrite(LED_R, HIGH);
      digitalWrite(BUZZER, HIGH);
      delay(1000);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_R, LOW);
      Serial.println("LOCK CLOSED");
      mfrc522.PCD_Init();   // Initiate MFRC522
  
  
    }
    }else{
      return;
    }
  }
  else{
    }  
}

void loop(){

  handle_rfid();
  handle_swipe();
}

```

## 3.7 References & Credits

https://www.youtube.com/watch?v=doe1aw7sorU