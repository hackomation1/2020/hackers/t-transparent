# 4. Business & Marketing

## 4.1 Objectives

The objectives are convenience for the user, but also avoid contact with the door handle and people as much as possible (COVID-19). By having a keycard with a tag or the use of a keypad in case the user forget their keycard.
 (for example: we don't have to ask the caretaker or concierge for the keys. You have your own key, in the form of a tag(card). You can put this tag in a card or in your key ring.)
The IOT in your mobile can also allow you to open the door. Even if you are not on the spot, and want to grant someone access to enter the space
The door can also be opened from the inside, without a key or pin code. Simply by waving your hand at the Ultrasonic Sensor.
In conclusion we want to make life easier and less stressful for the user, because hours of searching for your keys are frustrating.

## 4.3 Ananlyze your market & segments

The markets that being targeted are:

1. Tourism sector
2. Commercial sector 
3. Goverment offices
4. Businesses

With these following applications:

1. This project can also serve as an early morning employee clock-in.
   At which the fingerprint sensor can also be avoided.

2. The possibility to open a lock remotely.
   a. pets/farms
   b. old people/ people with mobility problems
   c. inhouse-office

3. Inland at recreational resorts or guest houses, you do not have proper access to electricity. 
   Especially if you are going to stay somewhere, where the houses and / or rooms are primitive. And you want to keep your         belongings well managed safe to prevent theft. The lock can work without direct connection to the power supply. The lock can also work without WIFI, but also with bluetooth.

4. The RFID tag technology also serves as a convenience for anti theft systems in shops, by placing the tags on your clothing.


## 4.4 Build your business canvas

<With the help or (Canvanizer Online)[https://canvanizer.com/choose/business-model-canvases] create a business canvas for your project>

Click on **link ** to see our  business canvas

[link](https://canvanizer.com/canvas/rsnprCBwdOrML)

## 2.5 Build your pitch deck

< 1. Build your Problem Solution Pitch for Idea Pitching!>
< 2. Build pitch deck for products/investor pitch>

Click on **link ** to see our pitch deck

[link](https://canvanizer.com/canvas/rA0e2u8NFcvSt)

## 4.6 Make a project Poster

![t-transparent](img/Poster.png)

## 4.7 Files & Code

The code used in the demo

```
#include <SPI.h>
#include <MFRC522.h>
#include <ESP32Servo.h>



//ULTRASONIC
int TRIG_PIN  = 17; // Arduino pin connected to Ultrasonic Sensor's TRIG pin
int ECHO_PIN  = 34; // Arduino pin connected to Ultrasonic Sensor's ECHO pin
int DISTANCE_THRESHOLD = 10; // centimeters

// variables will change:
float duration_us, distance_cm;

//LED & LOCK PIN DEF 
#define SS_PIN     21
#define RST_PIN    22
#define LED_G      5 //define green LED pin
#define LED_R      4 //define red LED
#define  BUZZER     2  //BUZZER pin
#define LOCK_PIN     15 //define red LED

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

void setup(){
  Serial.begin(9600);
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522

  
  //Led and & Lock def
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  
  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);
  pinMode(BUZZER, OUTPUT);

  //ULTRASONIC
  pinMode(TRIG_PIN, OUTPUT);  // set arduino pin to output mode
  pinMode(ECHO_PIN, INPUT);   // set arduino pin to input mode
  
}


void handle_swipe() {
  // generate 10-microsecond pulse to TRIG pin
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);


  // measure duration of pulse from ECHO pin
  duration_us = pulseIn(ECHO_PIN, HIGH);
  // calculate the distance
  distance_cm = 0.017 * duration_us;

  if(distance_cm < DISTANCE_THRESHOLD && distance_cm >1)
  {
      Serial.println("ULTRA Authorized access");
      digitalWrite(LED_G, HIGH);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      delay(500);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      Serial.println("LOCK RESET");
      delay(5000);

  }
}


void handle_rfid(){
  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent()) 
  {
    // Select one of the cards
    if ( mfrc522.PICC_ReadCardSerial()) 
    {
  
    //Show UID on serial monitor
    Serial.print("UID tag :");
    String content= "";
    byte letter;
    for (byte i = 0; i < mfrc522.uid.size; i++) 
    {
       Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
       Serial.print(mfrc522.uid.uidByte[i], HEX);
       content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
       content.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Message : ");
    content.toUpperCase();
    if (content.substring(1) == "59 F7 D8 C2") //change here the UID of the card/cards that you want to give access
    {
      Serial.println("Authorized access");
      //delay(500);
      digitalWrite(LED_G, HIGH);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      delay(1000);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      Serial.println("LOCK RESET");
      mfrc522.PCD_Init();   // Initiate MFRC522
    
  
    }
   else   {
      Serial.println(" Access denied");
      digitalWrite(LED_R, HIGH);
      digitalWrite(BUZZER, HIGH);
      delay(1000);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_R, LOW);
      Serial.println("LOCK CLOSED");
      mfrc522.PCD_Init();   // Initiate MFRC522
  
  
    }
    }else{
      return;
    }
  }
  else{
    }  
}

void loop(){

  handle_rfid();
  handle_swipe();
}

```
