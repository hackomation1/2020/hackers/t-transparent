# 2. Embedded solution

The purpose of the embedded solution is to open a door with a tag \ card (from outside) and a proximity sensor (from inside)

## 2.1 Objectives

The approach manner is, trying to make the components work one setup at a time and at the end putting them all together in one setup. The reason for this approach is so that we don't get confused and overwhelmed.  So first the RFID with the ESP32 and Micro servo lock only. Then the keypad, RFID, with the ESP32 and Micro servo lock. after that replacing the Micro serco lock with the Solenoid lock. Also putting the Ultrasonic proximity sensor in the setup. And trying to make it all work. 

## 2.3 Steps taken

The first step we did was soldered all the pins of the ESP32.

![t-transparent](img/3.png)

The second step was set up the ESP32 with all it's needed libraries
Set up ESP32: 
install ESP32 board in your Arduino IDE
- In your Arduino IDE, go to File> Preferences.
    Enter https://dl.espressif.com/dl/package_esp32_index.json 
- into the “Additional Board Manager URLs” field 
    Open the Boards Manager. Go to Tools > Board > Boards Manager…
- Search for ESP32 and press install button for the “ESP32 by Espressif Systems“

The third step was making a setup with the ESP32, RFID, Micro servo, 2 LED and a buzzer.

![t-transparent](img/4.png)

The  next step was trying putting the keypad in the setup also.

![t-transparent](img/5.png)

The fifth step was to replace the micro servo lock with the solenoid lock.

After that, the next step then was also putting in the Ultrasonic proximity sensor. 

Also putting the enclosure together.

![t-transparent](img/6.png)

![t-transparent](img/7.png)

![t-transparent](img/18.png)

![t-transparent](img/19.png)


## 2.4 Testing & Problems

issues we came across:

1. The codes used with the Arduino UNO differ a lot from the codes of ESP32. The reason for this is because ESP32 is a different board and has different pins that have 2 functions. The librabies are a bit different for the esp32, they have to be installed first in order to use the board

2. The tricky part is that when uploading a code, the boot button must be held everytime , otherwise the upload will not be completed

3. The USB mounting port is very sensitive and breaks easily. With our first ESP32, the USB mounting port broke, before the finale.
we got a new one(second)

4. The transition form the micro servo lock to a solenoid lock was difficult, because each has its own coding. The compilers like 
digitalWrite, int, #define certainly had to be taken into account. Because of the case sensitivity.

5. The micro servo has an advantage that it can operate without additional voltage supply. The solenoid lock needs a source of 9-12 volts, and a relay, these items may take up some space. 

6. The code for the OLED, was a bit tricky. Because the OLED is built-in, so the pins was a bit hard to define.(we decided to leave the OLED out).

7. More pins could be available for our ESP32, maybe one that we could customize ourselves. This is because the OLED built in  screen is quite small, and we wanted to put a larger control panel, interfacing screen for the user.
although the LCD programming is also a bit more complicated. So the Built-in OLED has its pros and cons.

8. Sometimes the RFID didn't work when we were testing the setup, after some exspection it turned out that you shouldn't test the RFID in the turn of metal (in our case a metal table).

9. Always check that the wiring is correct, because sometimes the code and setup may be correct. But the wiring is not properly in place.

10. With the Ultrasonic proximity sensor there was initially a delay of 500 milliseconds, the setup did not work then. We changed the delay from 500 milliseconds to 5000 milliseconds. And then the setup worked.

11. Our goal was to have a keypad, RFID and Ultrasonic proximity sensor in the setup. Because the codes of the keypad and of the Ultrasonic proximity sensor clash against each other, we removed the keypad from the project. **the keypad - RFID kan be seen as a daughter project**

13. Some of the modules like the RFID, Sensors and the Relay draw a lot of current. This results in a low power supply to the Relay,as a result,it does not switch the lock open and closed.

Solution** I put an extra power source that could deliver 3.3V. Thus een Arduino Uno

14. The lock didn't go on and off, although the LED's and RFID were reacting. 

** We put Brake fluid in the solenoid lock

**Can RFID be hacked?**
RFID  runs on different frequencies ,which the hacker can not track all
There haven’t been any documented cases 

How to prevent RFID hacking?
1. SHAI used DUMPINFO and READ AND WRITE first to write my DATA on it, for example my NAME 
2. You can use  RFID sleeve 
3. Permantly locked Tag data 
4. SINGLE source Password protection
5. Mult-Point Password protection

**In the future this project may also have access records related to time and name.

**The project is a mix of homeautomation and security.

## 2.5 Proof of work
 
A video seeing the Solenoid Lock working:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jLboGMT_NHY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


A video seeing the keypad working:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QDZpc-_ZAJ0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


A  short video of the setup with the components: ESP32-RFID-Ultrasonic proximity sensor-Buzzer-4 LED-Relay-Solenoid lock- battery 

<iframe width="560" height="315" src="https://www.youtube.com/embed/x2T7EOsQlIk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/niu5lz-8cgA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![t-transparent](img/14.png)

![t-transparent](img/15.png)

![t-transparent](img/17.png)

![t-transparent](img/16.png)

Keypad lockfor vault demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/BtKf1xb54eA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![t-transparent](img/20.png)

## 2.6 Files & Code

The code used for the setup: ESP32-RFID-Ultrasonic proximity sensor-Buzzer-4 LED-Relay-Solenoid lock- battery

```
#include <SPI.h>
#include <MFRC522.h>
#include <ESP32Servo.h>
//#include <Keypad.h>

//const int ROW_NUM = 4; //four rows
//const int COLUMN_NUM = 4; //three columns

//ULTRASONIC
int TRIG_PIN  = 17; // Arduino pin connected to Ultrasonic Sensor's TRIG pin
int ECHO_PIN  = 34; // Arduino pin connected to Ultrasonic Sensor's ECHO pin
int DISTANCE_THRESHOLD = 10; // centimeters

// variables will change:
float duration_us, distance_cm;

//LED & LOCK PIN DEF 
#define SS_PIN     21
#define RST_PIN    22
#define LED_G      5 //define green LED pin
//#define LED_B    35 //define green LED pin
#define LED_R      4 //define red LED
#define  BUZZER     2  //BUZZER pin
#define LOCK_PIN     15 //define red LED

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

//char keys[ROW_NUM][COLUMN_NUM] = {
//{'1','2','3','A'},
//{'4','5','6','B'},
//{'7','8','9','C'},
//{'*','0','#','D'}
//};

//byte pin_rows[ROW_NUM] = {13, 12, 14, 27}; //connect to the row pinouts of the keypad
//byte pin_column[COLUMN_NUM] = {26, 25, 33,32}; //connect to the column pinouts of the keypad

//Keypad keypad = Keypad( makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM );

//const String password = "1234"; // change your password here
//String input_password;

void setup(){
  Serial.begin(9600);
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  //input_password.reserve(32); // maximum input characters is 33, change if needed

  //Led and & Lock def
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  
  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);
  pinMode(BUZZER, OUTPUT);

  //ULTRASONIC
  pinMode(TRIG_PIN, OUTPUT);  // set arduino pin to output mode
  pinMode(ECHO_PIN, INPUT);   // set arduino pin to input mode
  
}

//
//void handle_keypad(char key){
//  //char key = keypad.getKey();
//  if (key){
//    Serial.println(key);
//    if(key == '*') {
//      input_password = ""; // clear input password
//    } else if(key == '#') {
//      if(password == input_password) {
//        Serial.println("password is correct");
//        // DO YOUR WORK HERE GREEN LED ON LOCK OPEN BUZ ON
//        digitalWrite(LED_G, HIGH);
//        Serial.println("BUZ ON");
//        digitalWrite(BUZZER, HIGH);
//        digitalWrite(LOCK_PIN, HIGH);
//        delay(1000);
//        Serial.println("BUZ OFF");
//        digitalWrite(BUZZER, LOW);
//        digitalWrite(LOCK_PIN, LOW);
//        digitalWrite(LED_G, LOW);
//        Serial.println("LOCK CLOSED");
//        delay(300);   
//      } else {
//         Serial.println("password is incorrect, try again");
//         // DO YOUR WORK HERE RED LED ON LOCK OPEN BUZ  ON AND LOCK CLOSED
//         digitalWrite(LED_R, HIGH);
//        Serial.println("BUZ ON");
//        digitalWrite(BUZZER, HIGH);
//        delay(1000);
//         Serial.println("BUZ OFF");
//         digitalWrite(BUZZER, LOW);
//         digitalWrite(LOCK_PIN, LOW);
//         digitalWrite(LED_R, LOW);
//         Serial.println("LOCK CLOSED");
//         delay(300);   
//        
//      }
//
//      input_password = ""; // clear input password
//    } else {
//      input_password += key; // append new character to input password string
//    }
//  } 
//  
// }


void handle_swipe() {
  // generate 10-microsecond pulse to TRIG pin
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  //delay(5000);

  // measure duration of pulse from ECHO pin
  duration_us = pulseIn(ECHO_PIN, HIGH);
  // calculate the distance
  distance_cm = 0.017 * duration_us;

  if(distance_cm < DISTANCE_THRESHOLD)
  {
      Serial.println("ULTRA Authorized access");
      digitalWrite(LED_G, HIGH);
      //delay(500);
      //digitalWrite(LED_B, HIGH);
      //delay(500);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      delay(500);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      //delay(500);
      //digitalWrite(LED_B, LOW);
      //delay(500);
      Serial.println("LOCK RESET");
      delay(5000);

  }
//    else
//        digitalWrite(LED_R, HIGH);
//        delay(500);
////      //digitalWrite(LED_B, HIGH);
////      //delay(500);
//        digitalWrite(BUZZER, HIGH);
//        digitalWrite(LOCK_PIN,LOW);
////      Serial.println("LOCK CLOSED");
//       delay(500);
//       digitalWrite(BUZZER, LOW);
//       digitalWrite(LOCK_PIN, LOW);
//       digitalWrite(LED_R, LOW);
////      //delay(500);
////      //digitalWrite(LED_B, LOW);
////      //delay(500);
////      Serial.println("LOCK RESET");
////      //delay(500); 
   }


void handle_rfid(){
  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent()) 
  {
    // Select one of the cards
    if ( mfrc522.PICC_ReadCardSerial()) 
    {
  
    //Show UID on serial monitor
    Serial.print("UID tag :");
    String content= "";
    byte letter;
    for (byte i = 0; i < mfrc522.uid.size; i++) 
    {
       Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
       Serial.print(mfrc522.uid.uidByte[i], HEX);
       content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
       content.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Message : ");
    content.toUpperCase();
    if (content.substring(1) == "59 F7 D8 C2") //change here the UID of the card/cards that you want to give access
    {
      Serial.println("Authorized access");
      //delay(500);
      digitalWrite(LED_G, HIGH);
      digitalWrite(BUZZER, HIGH);
      digitalWrite(LOCK_PIN, HIGH);
      Serial.println("LOCK OPEN");
      //myServo.write(180);
      delay(1000);
      //myServo.write(0);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_G, LOW);
      Serial.println("LOCK RESET");
      mfrc522.PCD_Init();   // Initiate MFRC522
      //delay(500);
  
    }
   else   {
      Serial.println(" Access denied");
      digitalWrite(LED_R, HIGH);
      digitalWrite(BUZZER, HIGH);
      delay(1000);
      digitalWrite(BUZZER, LOW);
      digitalWrite(LOCK_PIN, LOW);
      digitalWrite(LED_R, LOW);
      Serial.println("LOCK CLOSED");
      mfrc522.PCD_Init();   // Initiate MFRC522
     // delay(500);
  
    }
    }else{
      return;
    }
  }
  else{
    }  
}

void loop(){
//  char key = keypad.getKey();
//  if (key) {
//    handle_keypad(key);
//  }
  handle_rfid();
  handle_swipe();
}

```

**Team T-Transparent Keypad doorlock MINI project. You can use this for your safe or vault. Or just a doorlock inside or outside home or appartment.**
```
#include <Keypad.h>

const int ROW_NUM = 4; //four rows
const int COLUMN_NUM = 4; //three columns

//LED & LOCK PIN DEF 
#define LED_G      5 //define green LED pin
#define LED_R      4 //define red LED
//#define BUZZER     2 //buzzer pin
const int  buzzer =  2; //buzzer pin
#define LOCK_PIN     15 //define red LED

char keys[ROW_NUM][COLUMN_NUM] = {
{'1','2','3','A'},
{'4','5','6','B'},
{'7','8','9','C'},
{'*','0','#','D'}
};

byte pin_rows[ROW_NUM] = {13, 12, 14, 27}; //connect to the row pinouts of the keypad
byte pin_column[COLUMN_NUM] = {26, 25, 33,32}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM );

const String password = "1234"; // change your password here
String input_password;

void setup(){
  Serial.begin(9600);
  input_password.reserve(32); // maximum input characters is 33, change if needed

  //Led and & Lock def
  pinMode(LED_G, OUTPUT);
  pinMode(LED_R, OUTPUT);
  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);
  pinMode(buzzer, OUTPUT);
}

void loop(){
  char key = keypad.getKey();

  if (key){
    Serial.println(key);

    if(key == '*') {
      input_password = ""; // clear input password
    } else if(key == '#') {
      if(password == input_password) {
        Serial.println("password is correct");
        // DO YOUR WORK HERE GREEN LED ON LOCK OPEN BUZ ON
        digitalWrite(LED_G, HIGH);
        Serial.println("BUZ ON");
        digitalWrite(buzzer, HIGH);
        delay(500);
         Serial.println("BUZ OFF");
         digitalWrite(buzzer, LOW);
        digitalWrite(LOCK_PIN, HIGH);
        Serial.println("LOCK OPEN");
        //myServo.write(180);
        delay(5000);
        //myServo.write(0);
        digitalWrite(LOCK_PIN, LOW);
        digitalWrite(LED_G, LOW);
        Serial.println("LOCK RESET");
        
      } else {
         Serial.println("password is incorrect, try again");
         // DO YOUR WORK HERE RED LED ON LOCK OPEN BUZ  ON AND LOCK CLOSED
        digitalWrite(LED_R, HIGH);
        Serial.println("BUZ ON");
        digitalWrite(buzzer, HIGH);
        delay(500);
        Serial.println("BUZ OFF");
        digitalWrite(buzzer, LOW);
        digitalWrite(LOCK_PIN, LOW);
        Serial.println("LOCK CLOSED");
        delay(5000);
        digitalWrite(LED_R, LOW);
          
        
      }

      input_password = ""; // clear input password
    } else {
      input_password += key; // append new character to input password string
    }
  }
}
```

## 2.7 References & Credits

Set up ESP32: Reference: sumukhainfotech.com/installing-esp32-for-arduino-ide/

https://www.instructables.com/ESP32-With-RFID-Access-Control/

https://home.et.utwente.nl/slootenvanf/2019/11/19/connected-distance-sensor-esp32-module/

https://github.com/espressif/arduino-esp32

https://github.com/miguelbalboa/rfid/blob/master/examples/DumpInfo/DumpInfo.ino

https://github.com/me-no-dev/arduino-esp32fs-plugin

https://github.com/me-no-dev/AsyncTCP/find/master

https://www.euro-locks.be/blog/advantages-of-rfid-electronic-door-locks

https://robotzero.one/esp32-face-door-entry/

https://www.arduino.cc/

https://www.espressif.com/en/products/socs/esp32